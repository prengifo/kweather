package com.patrick.rengifo.kweather

import android.location.Location
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.patrick.rengifo.kweather.remote.WeatherApi
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by Patrick Rengifo on 2020-02-19.
 */
@RunWith(AndroidJUnit4::class)
class WeatherApiTest {
    @Test
    fun useKlarnaHQ() {
        val location = Location("passive")
        location.latitude = 59.337239
        location.longitude = 18.062381
        val response = WeatherApi.getForecast(location)
        Assert.assertNotEquals(null, response)
        Assert.assertEquals(59.337239, response?.latitude)
        Assert.assertEquals(18.062381, response?.longitude)
    }

    @Test
    fun haveHourlyData() {
        val location = Location("passive")
        location.latitude = 59.337239
        location.longitude = 18.062381
        val response = WeatherApi.getForecast(location)
        Assert.assertNotEquals(null, response)
        Assert.assertNotEquals(null, response?.hourly)
        Assert.assertNotEquals(0, response?.hourly?.data?.size)
        Assert.assertEquals(59.337239, response?.latitude)
        Assert.assertEquals(18.062381, response?.longitude)
    }
}
