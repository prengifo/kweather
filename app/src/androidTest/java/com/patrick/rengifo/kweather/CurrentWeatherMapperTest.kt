package com.patrick.rengifo.kweather

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.patrick.rengifo.kweather.models.CurrentWeatherMapper
import com.patrick.rengifo.kweather.remote.Currently
import com.patrick.rengifo.kweather.remote.Hourly
import com.patrick.rengifo.kweather.remote.WeatherResponse
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by Patrick Rengifo on 2020-02-19.
 */
@RunWith(AndroidJUnit4::class)
class CurrentWeatherMapperTest {
    @Test
    fun currentWeatherMapperTest() {
        val currently = Currently(
            summary = "Clear",
            time = 1582135630,
            precipProbability = 0.0,
            temperature = 36.43,
            apparentTemperature = 32.21,
            windSpeed = 5.09
        )
        val hourly = Hourly(
            summary = "Clear",
            icon = "partly-cloudy-day",
            data = listOf(currently)
        )
        val weatherResponse = WeatherResponse(
            currently = currently,
            hourly = hourly
        )
        val currentWeather = CurrentWeatherMapper.fromWeatherResponse(weatherResponse)
        Assert.assertEquals("Prop. of rain: 0.0 %", currentWeather?.chanceOfRain)
        Assert.assertEquals("Currently: Clear", currentWeather?.current)
        Assert.assertEquals("Temperature: 36.43°", currentWeather?.temperature)
        Assert.assertEquals("Feels like: 32.21°", currentWeather?.feelsLike)
        Assert.assertEquals("Wind speed: 5.09 m/s", currentWeather?.windSpeed)
        Assert.assertEquals("07:07 PM: Clear, Temperature: 36.43°", currentWeather?.hourlyWeather?.first())
    }
}