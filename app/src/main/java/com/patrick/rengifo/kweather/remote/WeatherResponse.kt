package com.patrick.rengifo.kweather.remote

import com.squareup.moshi.JsonClass

/**
 * Created by Patrick Rengifo on 2020-02-19.
 */
@JsonClass(generateAdapter = true)
data class WeatherResponse(
    val latitude : Double = 0.0,
    val longitude : Double = 0.0,
    val timezone : String = "Europe/Stockholm",
    val currently : Currently?,
    val hourly: Hourly?
)

@JsonClass(generateAdapter = true)
data class Hourly(
    val summary: String?,
    val icon: String?,
    val data: List<Currently> = listOf()
)

@JsonClass(generateAdapter = true)
data class Currently (
    val time : Long,
    val summary : String?,
    val icon : String = "",
    val precipIntensity : Double = 0.0,
    val precipProbability : Double?,
    val temperature : Double?,
    val apparentTemperature : Double?,
    val dewPoint : Double = 0.0,
    val humidity : Double = 0.0,
    val pressure : Double = 0.0,
    val windSpeed : Double?,
    val windGust : Double = 0.0,
    val windBearing : Int = 0,
    val cloudCover : Double = 0.0,
    val uvIndex : Int = 0,
    val visibility : Double = 0.0,
    val ozone : Double = 0.0
)