package com.patrick.rengifo.kweather.models

import android.text.format.DateFormat
import com.patrick.rengifo.kweather.remote.WeatherResponse
import java.util.*

/**
 * Created by Patrick Rengifo on 2020-02-19.
 */
object CurrentWeatherMapper {
    fun fromWeatherResponse(weatherResponse: WeatherResponse): CurrentWeather? {
        var current: String? = ""
        var lastUpdated: String? = ""
        var temperature: String? = ""
        var feelsLike: String? = ""
        var chanceOfRain: String? = ""
        var windSpeed: String? = ""
        weatherResponse.currently?.let { currentWeather ->
            current = "Currently: ${currentWeather.summary}"
            lastUpdated = "Last updated on: ${getDate(currentWeather.time)}"
            temperature = getTemperature(currentWeather.temperature)
            feelsLike = if (currentWeather.apparentTemperature != null) {
                "Feels like: ${currentWeather.apparentTemperature}°"
            } else {
                null
            }
            chanceOfRain = if (currentWeather.precipProbability != null) {
                "Prop. of rain: ${currentWeather.precipProbability} %"
            } else {
                null
            }
            windSpeed = if (currentWeather.windSpeed != null) {
                "Wind speed: ${currentWeather.windSpeed} m/s"
            } else {
                null
            }
        }
        val hourly: List<String> = weatherResponse.hourly?.data?.map {
            "${getHours(it.time)}: ${it.summary}, ${getTemperature(it.temperature)}"
        } ?: listOf()
        return CurrentWeather(
            lastUpdated,
            current,
            temperature,
            feelsLike,
            chanceOfRain,
            windSpeed,
            hourly
        )
    }

    private fun getDate(timestamp: Long): String {
        val calendar = Calendar.getInstance(Locale.ENGLISH)
        calendar.timeInMillis = timestamp * 1000L
        return DateFormat.format("dd-MM-yyyy", calendar).toString()
    }

    private fun getHours(timestamp: Long): String {
        val calendar = Calendar.getInstance(Locale.ENGLISH)
        calendar.timeInMillis = timestamp * 1000L
        return DateFormat.format("hh:mm a", calendar).toString()
    }

    private fun getTemperature(temperature: Double?): String? {
        return if (temperature != null) {
            "Temperature: ${temperature}°"
        } else {
            null
        }
    }
}