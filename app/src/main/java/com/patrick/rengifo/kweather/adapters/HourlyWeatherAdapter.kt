package com.patrick.rengifo.kweather.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.patrick.rengifo.kweather.R
import kotlinx.android.synthetic.main.my_text_view.view.*

/**
 * Created by Patrick Rengifo on 2020-03-25.
 */
class HourlyWeatherAdapter(private val hourlyWeather: List<String>) : RecyclerView.Adapter<HourlyWeatherAdapter.HourlyWeatherViewHolder>() {

    class HourlyWeatherViewHolder(val parent: View) : RecyclerView.ViewHolder(parent)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HourlyWeatherViewHolder {
        val parentLayout = LayoutInflater.from(parent.context)
            .inflate(R.layout.my_text_view, parent, false) as ConstraintLayout
        return HourlyWeatherViewHolder(parentLayout)
    }

    override fun getItemCount(): Int = hourlyWeather.size

    override fun onBindViewHolder(holder: HourlyWeatherViewHolder, position: Int) {
        val weather = hourlyWeather[position]
        holder.parent.textView.text = weather
    }
}