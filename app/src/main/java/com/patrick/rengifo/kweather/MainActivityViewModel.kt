package com.patrick.rengifo.kweather

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.patrick.rengifo.kweather.models.CurrentWeather
import com.patrick.rengifo.kweather.models.CurrentWeatherMapper
import com.patrick.rengifo.kweather.remote.WeatherApi
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import java.io.IOException

/**
 * Created by Patrick Rengifo on 2020-02-19.
 */
class MainActivityViewModel {

    private var weatherRequest: Call? = null
    private var userLocation: Location? = null
    private var fusedLocationClient: FusedLocationProviderClient? = null
    val currentWeather: MutableLiveData<CurrentWeather> = MutableLiveData()
    val hourlyWeather: MutableList<String> = mutableListOf()

    @SuppressLint("MissingPermission")
    fun getUserLocation(context: Context) {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)
        fusedLocationClient?.lastLocation
            ?.addOnSuccessListener { location: Location? ->
                // Got last known location. In some rare situations this can be null.
                print(location.toString())
                if (location != null) {
                    userLocation = location
                } else {
                    userLocation = Location("").also { it.altitude = 59.3293; it.longitude = 18.0686 }
                }
                getCurrentWeather()
            }
            ?.addOnFailureListener {
                print(it.toString())
            }
    }

    fun onPause() {
        weatherRequest?.cancel()
    }

    fun onDestroy(activity: AppCompatActivity) {
        currentWeather.removeObservers(activity)
    }

    private fun getCurrentWeather() {
        userLocation?.let {
            weatherRequest = WeatherApi.getForecastAsync(it)
            weatherRequest?.enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    e.printStackTrace()
                }

                override fun onResponse(call: Call, response: Response) {
                    response.use {
                        if (!response.isSuccessful) throw IOException("Unexpected code $response")

                        WeatherApi.parseResponse(response)?.let { parsedJson ->
                            val newCurrentWeather = CurrentWeatherMapper.fromWeatherResponse(parsedJson)
                            currentWeather.postValue(newCurrentWeather)
                            hourlyWeather.clear()
                            hourlyWeather.addAll(newCurrentWeather?.hourlyWeather ?: mutableListOf())
                        }
                    }
                }
            })
        }
    }

}