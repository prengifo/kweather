package com.patrick.rengifo.kweather.remote

import android.location.Location
import com.squareup.moshi.Moshi
import okhttp3.Call
import okhttp3.HttpUrl.Companion.toHttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response


/**
 * Created by Patrick Rengifo on 2020-02-19.
 */
object WeatherApi {

    private val client = OkHttpClient()
    private val moshi = Moshi.Builder().build()
    private val weatherJsonAdapter = moshi.adapter(WeatherResponse::class.java)

    fun getForecast(location: Location): WeatherResponse? {
        val request = createRequest(location)

        val response = client.newCall(request).execute()

        return parseResponse(response)
    }

    fun parseResponse(response: Response): WeatherResponse? {
        return weatherJsonAdapter.fromJson(response.body!!.source())
    }

    fun getForecastAsync(location: Location): Call {
        val request = createRequest(location)

        return client.newCall(request)
    }

    private fun createRequest(location: Location): Request {
        val locationString = "${location.latitude},${location.longitude}"

        val urlBuilder =
            "https://api.darksky.net/forecast/".toHttpUrl().newBuilder()
        urlBuilder.addPathSegments("2bb07c3bece89caf533ac9a5d23d8417")
        urlBuilder.addPathSegment(locationString)
        urlBuilder.addQueryParameter("units", "si")
        val url = urlBuilder.build().toString()

        return Request.Builder()
            .url(url)
            .build()
    }
}