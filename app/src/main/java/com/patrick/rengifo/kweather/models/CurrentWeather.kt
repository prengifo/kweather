package com.patrick.rengifo.kweather.models

/**
 * Created by Patrick Rengifo on 2020-02-19.
 */
data class CurrentWeather(
    val lastUpdated: String? = null,
    val current: String? = null,
    val temperature: String? = null,
    val feelsLike: String? = null,
    val chanceOfRain: String? = null,
    val windSpeed: String? = null,
    val hourlyWeather: List<String> = listOf()
)

data class HourlyWeather(
    val temperature: String? = null,
    val summary: String? = null
)