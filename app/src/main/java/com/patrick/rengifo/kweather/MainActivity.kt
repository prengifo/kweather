package com.patrick.rengifo.kweather

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.patrick.rengifo.kweather.adapters.HourlyWeatherAdapter
import com.patrick.rengifo.kweather.models.CurrentWeather
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    companion object {
        private const val REQUEST_LOCATION = 1
    }

    private var mainActivityViewModel: MainActivityViewModel = MainActivityViewModel()

    private lateinit var viewAdapter: HourlyWeatherAdapter
    private lateinit var viewManager: RecyclerView.LayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainActivityViewModel = MainActivityViewModel()
        val currentWeatherObserver = Observer<CurrentWeather> { newWeather -> setUpUI(newWeather) }
        mainActivityViewModel.currentWeather.observe(this@MainActivity, currentWeatherObserver)

        if (ContextCompat.checkSelfPermission(
                this@MainActivity,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            // Permission is not granted
            ActivityCompat.requestPermissions(
                this@MainActivity,
                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
                REQUEST_LOCATION
            )
        } else {
            mainActivityViewModel.getUserLocation(this@MainActivity)
        }

        viewManager = LinearLayoutManager(this)
        hoursRecyclerView.layoutManager = viewManager
        viewAdapter = HourlyWeatherAdapter(mainActivityViewModel.hourlyWeather)
        hoursRecyclerView.adapter = viewAdapter
    }

    override fun onPause() {
        super.onPause()
        mainActivityViewModel.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        mainActivityViewModel.onDestroy(this@MainActivity)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_LOCATION -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    mainActivityViewModel.getUserLocation(this@MainActivity)
                }
                return
            }
            else -> {
                // Ignore all other requests.
            }
        }
    }

    private fun setUpUI(currentWeather: CurrentWeather) {
        weatherSummary.text = currentWeather.current
        weatherTemperature.text = currentWeather.temperature
        weatherFeelsLike.text = currentWeather.feelsLike
        weatherChanceOfRain.text = currentWeather.chanceOfRain
        weatherWindSpeed.text = currentWeather.windSpeed
        weatherLastUpdated.text = currentWeather.lastUpdated

        viewAdapter.notifyDataSetChanged()
    }
}
